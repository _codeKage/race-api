<?php

namespace App\models;

class User {
    private $id;
    private $name;
    private $email;
    private $password;
    private $api_token;
    private $created_at;

    public function __construct($id = null, $name = null, $email = null, $password = null, $api_token = null, $created_at = null)
    {
        $this->id = $id;
        $this->api_token = $api_token;
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->created_at = $created_at;
    }

    public function getId() {
        return $this->id;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setEmail($email) {
        $this->email = $email;
        return $this;
    }

    public function getEmail() {
        return $this->email;
    }

    public function setPassword($password) {
        $this->password = $password;
        return $this;
    }

    public function getPassword() {
        return $this->password;
    }

    public function setApiToken($api_token) {
        $this->api_token = $api_token;
        return $this;
    }

    public function getApiToken() {
        return $this->api_token;
    }

    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }

}