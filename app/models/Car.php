<?php

namespace App\models;

class Car {
    private $id;
    private $car_type;
    private $points_follow;
    private $speed;
    private $race_track;

    public function __construct($id = null, $car_type = null, $points_follow = null, $speed = null, $race_track = null)
    {
        $this->id = $id;
        $this->car_type = $car_type;
        $this->points_follow = $points_follow;
        $this->speed = $speed;
        $this->race_track = $race_track;
    }

    public function getId() {
        return $this->id;
    }

    public function setCarType($car_type) {
        $this->car_type = $car_type;
        return $this;
    }

    public function getCarType() {
        return $this->car_type;
    }

    public function setPointsFollow($points_follow) {
        $this->points_follow = $points_follow;
        return $this;
    }

    public function getPointsFollow() {
        return $this->points_follow;
    }

    public function setSpeed($speed) {
        $this->speed = $speed;
        return $this;
    }

    public function getSpeed() {
        return $this->speed;
    }

    public function setRaceTrack(RaceTrack $race_track) {
        $this->race_track = $race_track;
        return $this;
    }

    public function getRaceTrack() {
        return $this->race_track;
    }
}