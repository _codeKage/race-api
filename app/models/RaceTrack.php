<?php

namespace App\models;

class RaceTrack {
    private $id;
    private $name;
    private $description;
    private $tiles_coords;
    private $user;
    private $created_at;

    public function __construct($id = null, $name, $description, $tiles_coords, $user, $created_at = null)
    {
        $this->id = $id;
        $this->name = $name;
        $this->description = $description;
        $this->tiles_coords = $tiles_coords;
        $this->user = $user;
        $this->created_at = $created_at;
    }

    public function getId() {
        return $this->id;
    }

    public function setName($name) {
        $this->name = $name;
        return $this;
    }

    public function getName() {
        return $this->name;
    }

    public function setDescription($description) {
        $this->description = $description;
        return $this;
    }

    public function getDescription() {
        return $this->description;
    }

    public function setTilesCoords($tiles_coords) {
        $this->tiles_coords = $tiles_coords;
        return $this;
    }

    public function getTilesCoords() {
        return $this->tiles_coords;
    }

    public function setUser(User $user) {
        $this->user = $user;
    }

    public function getUser() {
        return $this->user;
    }

    public function setCreatedAt($created_at) {
        $this->created_at = $created_at;
        return $this;
    }

    public function getCreatedAt() {
        return $this->created_at;
    }
}