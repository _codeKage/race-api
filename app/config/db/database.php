<?php

namespace App\config\db;

use PDO;

class Database {

    private $conn;

    function __construct() {
        $this->conn = new PDO('mysql:host=localhost;dbname=race', 'root', 'root');
    }

    public function getConnection() {
        return $this->conn;
    }
}