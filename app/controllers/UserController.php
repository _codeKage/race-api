<?php

namespace App\controller;

use App\config\db\Database;
use App\models\User;
use PDOException;

class UserController {

    private $connection;

    public function __construct(Database $connection)
    {
        $this->connection = $connection->getConnection();
    }


    public function create(User $user) {
        $password = password_hash($user->getPassword(), PASSWORD_BCRYPT);
        $insertStatement = $this->connection->prepare('INSERT INTO `users`(`name`,`email`,`password`,`api_token`,`created_at`) VALUES(:name, :email, :password, :api_token, :created_at)');
        $insertStatement->execute([
            'name' => $user->getName(),
            'email' => $user->getEmail(),
            'password' => $password,
            'api_token' => '',
            'created_at' => (new \DateTime())->format('Y-m-D H:i:s')
        ]);

        $userId = $this->connection->lastInsertId();
        http_response_code(201);
        return $this->getUserById($userId);
    }

    public function update(User $user, $userId) {

    }

    public function remove($userId) {

    }

    public function login($username, $password) {

    }

    public function logout(User $user) {

    }

    private function getUserById($userId) {
        try {
            $user = $this->connection->prepare('SELECT `id`,`name`,`email`,`api_token` FROM `users` WHERE id = :id LIMIT 1');
            $user->execute([
                'id' => $userId
            ]);

            return $user->fetch();
        } catch(PDOException $e) {
            http_response_code(404);
        }
    }
}